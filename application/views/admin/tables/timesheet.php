<?php

defined('BASEPATH') or exit('No direct script access allowed');



$aColumns = [
    'id',
    'created_at',
    'amount',
    'type',
    'task',
    'balance'
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'client_timesheets';

$join = [
    'JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'client_timesheets.client_id'
];

$where  = [];
$filter = [];



if ($clientid != '') {
    array_push($where, 'AND ' . db_prefix() . 'client_timesheets.client_id=' . $clientid);
}

//$aColumns = hooks()->apply_filters('invoices_table_sql_columns', $aColumns);

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);
//echo "<pre>";
//print_r($result);
//die;
$this->ci->db->select('SUM(amount) as total', false);
$this->ci->db->where('client_id', $clientid);
$this->ci->db->where('type', 1);
$deposite = $this->ci->db->get(db_prefix() . 'client_timesheets')->row_array();
$output  = $result['output'];
$rResult = $result['rResult'];
//echo "<pre>";
//print_r($rResult);
//die();
foreach ($rResult as $aRow) {
    $row = [];
    $onclick = "showEditModal('".$aRow['id']."','".$aRow['type']."','".$aRow['amount']."','".$aRow['task']."')";
    $numberOutput = '<a href="#" onclick="'.$onclick.'">' . _d($aRow['created_at']) . '</a>';
    $row[] = $numberOutput;

    $row[] = $aRow['type'] == 2 ? '<i class="fa fa-clock-o" aria-hidden="true"></i>' : '<i class="fa fa-credit-card" aria-hidden="true"></i>';
    $row[] = $aRow['task'];


    if($aRow['type'] == 2) {
        //task
        $row[] = '-'.app_format_money($aRow['amount'], 'USD');
    }
    else{
        //payment
        $row[] = '+'.app_format_money($aRow['amount'], 'USD');
    }

    $aRow['balance'] = !empty( $aRow['balance'] ) ? $aRow['balance'] : 0;
    $style = $aRow['balance'] < 0 ? "style='color:red'" : '';
    $row[] = "<span ".$style.">". app_format_money($aRow['balance'], 'USD')."</span>";

    //$row = hooks()->apply_filters('timesheet_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}

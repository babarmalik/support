<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionEdit = has_permission('tasks', '', 'edit');
$hasPermissionDelete = has_permission('tasks', '', 'delete');
$tasksPriorities = get_tasks_priorities();

$aColumns = [
    '1', // bulk actions
    db_prefix() . 'tasks.id as id',
    'status',
    db_prefix() . 'tasks.name as task_name',
    'rel_id',
    'startdate',
    'duedate',
    get_sql_select_task_asignees_full_names() . ' as assignees',
    '(SELECT GROUP_CONCAT(name SEPARATOR ",") FROM ' . db_prefix() . 'taggables JOIN ' . db_prefix() . 'tags ON ' . db_prefix() . 'taggables.tag_id = ' . db_prefix() . 'tags.id WHERE rel_id = ' . db_prefix() . 'tasks.id and rel_type="task" ORDER by tag_order ASC) as tags',
    'priority',
];

$sIndexColumn = 'id';
$sTable = db_prefix() . 'tasks';

$where = [];
$join = [];

include_once(APPPATH . 'views/admin/tables/includes/tasks_filter.php');

array_push($where, 'AND CASE WHEN rel_type="project" AND rel_id IN (SELECT project_id FROM ' . db_prefix() . 'project_settings WHERE project_id=rel_id AND name="hide_tasks_on_main_tasks_table" AND value=1) THEN rel_type != "project" ELSE 1=1 END');

$custom_fields = get_table_custom_fields('tasks');

foreach ($custom_fields as $key => $field) {
    $selectAs = (is_cf_date($field) ? 'date_picker_cvalue_' . $key : 'cvalue_' . $key);
    array_push($customFieldsColumns, $selectAs);
    array_push($aColumns, '(SELECT value FROM ' . db_prefix() . 'customfieldsvalues WHERE ' . db_prefix() . 'customfieldsvalues.relid=' . db_prefix() . 'tasks.id AND ' . db_prefix() . 'customfieldsvalues.fieldid=' . $field['id'] . ' AND ' . db_prefix() . 'customfieldsvalues.fieldto="' . $field['fieldto'] . '" LIMIT 1) as ' . $selectAs);
}

$aColumns = hooks()->apply_filters('tasks_table_sql_columns', $aColumns);

// Fix for big queries. Some hosting have max_join_limit
if (count($custom_fields) > 4) {
    @$this->ci->db->query('SET SQL_BIG_SELECTS=1');
}

$result = data_tables_init(
    $aColumns,
    $sIndexColumn,
    $sTable,
    $join,
    $where,
    [
        'rel_type',
        'rel_id',
        'recurring',
        tasks_rel_name_select_query() . ' as rel_name',
        'billed',
        '(SELECT staffid FROM ' . db_prefix() . 'task_assigned WHERE taskid=' . db_prefix() . 'tasks.id AND staffid=' . get_staff_user_id() . ') as is_assigned',
        get_sql_select_task_assignees_ids() . ' as assignees_ids',
        '(SELECT MAX(id) FROM ' . db_prefix() . 'taskstimers WHERE task_id=' . db_prefix() . 'tasks.id and staff_id=' . get_staff_user_id() . ' and end_time IS NULL) as not_finished_timer_by_current_staff',
        '(SELECT staffid FROM ' . db_prefix() . 'task_assigned WHERE taskid=' . db_prefix() . 'tasks.id AND staffid=' . get_staff_user_id() . ') as current_user_is_assigned',
        '(SELECT CASE WHEN addedfrom=' . get_staff_user_id() . ' AND is_added_from_contact=0 THEN 1 ELSE 0 END) as current_user_is_creator',
    ]
);

$output = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    $row[] = '<div class="checkbox"><input type="checkbox" value="' . $aRow['id'] . '"><label></label></div>';

    $row[] = '<a href="' . admin_url('tasks/view/' . $aRow['id']) . '" onclick="init_task_modal(' . $aRow['id'] . '); return false;">' . $aRow['id'] . '</a>';

    $canChangeStatus = ($aRow['current_user_is_creator'] != '0' || $aRow['current_user_is_assigned'] || has_permission('tasks', '', 'edit'));
    $status = get_task_status_by_id($aRow['status']);

    $outputStatus = '';
    $status_icons = array(
        '1' => 'fa fa-square-o',
        '2' => 'fa fa-user-circle-o',
        '3' => 'fa fa-road',
        '4' => 'fa fa-clock-o',
        '99' => 'fa fa-eye',
        '5' => 'fa fa-check-square-o',
    );
    $status_button = array(
        '1' => ' btn-default btn-xs   btn-not-started',
        '2' => ' btn-warning btn-xs ',
        '3' => ' btn-xs ',
        '4' => ' btn-info btn-xs ',
        '99' => ' btn-danger  btn-xs ',
        '5' => ' btn-success  btn-xs ',
    );
    $status_text_color = array(
        '1' => ' label-default text-muted ',
        '2' => ' label-warning text-warning ',
        '3' => ' label-testing ',
        '4' => ' label-primary text-primary ',
        '99' => ' label-danger text-danger ',
        '5' => ' label-success text-success ',
    );
    $outputStatus = '<div class="btn-group" >';
    $mainTitle = ' ';

    $mainClass = ' ';
    $mainIdFunctions = ' ';
    if ($aRow['status'] == 5) {
        $button_class = ($aRow['status'] == 5) ? $status_button[$aRow['status']] : "btn-default";
        $title = 'Completed';
        $mainTitle = $title;
        $mainClass = $button_class;
        $mainIdFunctions = 'id="btn_main_' . $aRow['id'] . '" onmouseleave="changeIconOut(5,`main_' . $aRow['id'] . '`)"  onmouseenter="changeIconIn(5,`main_' . $aRow['id'] . '`)" onclick="task_mark_as(1 ,' . $aRow['id'] . '); return false;"';
        $outputStatus .= '<button id="btn_' . $aRow['id'] . '" onmouseleave="changeIconOut(5,' . $aRow['id'] . ')"  onmouseenter="changeIconIn(5,' . $aRow['id'] . ')" onclick="task_mark_as(1 ,' . $aRow['id'] . '); return false;" type="button" class="btn ' . $button_class . ' btn-xs" data-toggle="tooltip" data-placement="top" 
                title="' . $title . '"><i class="' . $status_icons[5] . '"></i></button>';
    } else if ($aRow['status'] == 1) {
        $button_class = ($aRow['status'] == 1) ? $status_button[$aRow['status']] : "btn-default";
        $title = 'Not Started';
        $mainTitle = $title;
        $mainClass = $button_class;
        $mainIdFunctions = ' id="btn_main_' . $aRow['id'] . '" onmouseleave="changeIconOut(1,`main_' . $aRow['id'] . '`)"  onmouseenter="changeIconIn(1,`main_' . $aRow['id'] . '`)"  onclick="task_mark_as(5 ,' . $aRow['id'] . '); return false;"';
        $outputStatus .= '<button id="btn_' . $aRow['id'] . '" onmouseleave="changeIconOut(1,' . $aRow['id'] . ')"  onmouseenter="changeIconIn(1,' . $aRow['id'] . ')"  onclick="task_mark_as(5 ,' . $aRow['id'] . '); return false;" type="button" class="btn ' . $button_class . ' btn-xs" data-toggle="tooltip" data-placement="top" 
                title="' . $title . '"><i class="' . $status_icons[1] . '"></i></button>';
    } else {
        $button_class = ($aRow['status'] == 1) ? $status_button[$aRow['status']] : "btn-default";
        $title = _l('task_mark_as', 'Not Started');
        $mainTitle = 'Not Started';
        $mainClass = $button_class;
        $mainIdFunctions = ' id="btn_main_' . $aRow['id'] . '" onmouseleave="changeIconOut(1,`main_' . $aRow['id'] . '`)"  onmouseenter="changeIconIn(1,`main_' . $aRow['id'] . '`)"  onclick="task_mark_as(5 ,' . $aRow['id'] . '); return false;"';
        $outputStatus .= '<button id="btn_' . $aRow['id'] . '" onclick="task_mark_as(1 ,' . $aRow['id'] . '); return false;" type="button" class="btn ' . $button_class . ' btn-xs" data-toggle="tooltip" data-placement="top" 
                title="' . $title . '"><i class="' . $status_icons[1] . '"></i></button>';
    }

    if ($canChangeStatus) {
        foreach ($task_statuses as $taskChangeStatus) {
            if ($taskChangeStatus['id'] != 1 && $taskChangeStatus['id'] != 5) {
                $button_class = ($aRow['status'] == $taskChangeStatus['id']) ? $status_button[$aRow['status']] : "btn-default";
                $title = ($aRow['status'] == $taskChangeStatus['id']) ? $taskChangeStatus['name'] : _l('task_mark_as', $taskChangeStatus['name']);
                $outputStatus .= '<button onclick="task_mark_as(' . $taskChangeStatus['id'] . ',' . $aRow['id'] . '); return false;" type="button" class="btn ' . $button_class . ' btn-xs" data-toggle="tooltip" data-placement="top" 
                title="' . $title . '"><i class="' . $status_icons[$taskChangeStatus['id']] . '"></i></button>';

                if ($aRow['status'] == $taskChangeStatus['id']) {
                    $mainTitle = $taskChangeStatus['name'];
                    $mainClass = $button_class;
                }

            }
        }

    }
    $mainIcon = ($aRow['status'] == 5) ? 'fa fa-check-square-o' : 'fa fa-square-o';
    $outputStatus .= '</div><style> 
                        .btn-not-started{background-color: #FFFED1;} 
                        .btn-default{background-color: white;}
                        .label-testing{border:1px solid #654321; color:#654321}
                        .label{margin-top: 5px;}</style>';
    $outputStatus = '<div class="btn-group dropright" style="width: 160px;">
                          <button type="button" '.$mainIdFunctions.' class="btn btn-default" >
                          <i class="' .$mainIcon. '"></i></button>
                          <button type="button" class="btn btn-default dropdown-toggle px-3  dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                          </button>
                          
                         <ul class="dropdown-menu" style="min-width: auto !important; margin-top: 30px;">
                              <li class="">
                              ' . $outputStatus . '
                            </li>
                            </ul>
                        </div>
                        <br />
                        <span class="label inline-block '.$status_text_color[$aRow['status']].'" > ' . $mainTitle . '</span>' ;

    $row[] = $outputStatus;

    $outputName = '';

    if ($aRow['not_finished_timer_by_current_staff']) {
        $outputName .= '<span class="pull-left text-danger"><i class="fa fa-clock-o fa-fw"></i></span>';
    }

    $outputName .= '<a href="' . admin_url('tasks/view/' . $aRow['id']) . '" class="display-block main-tasks-table-href-name' . (!empty($aRow['rel_id']) ? ' mbot5' : '') . '" onclick="init_task_modal(' . $aRow['id'] . '); return false;">' . $aRow['task_name'] . '<div class="hero" title="comments" style="border-radius: 5px;" >'.count_task_coments_by_id($aRow['id']).'</div></a>';

    if ($aRow['rel_name']) {
        $relName = task_rel_name($aRow['rel_name'], $aRow['rel_id'], $aRow['rel_type']);

        $link = task_rel_link($aRow['rel_id'], $aRow['rel_type']);

        $outputName .= '<span class="hide"> - </span><a class="text-muted task-table-related" data-toggle="tooltip" title="' . _l('task_related_to') . '" href="' . $link . '">' . $relName . '</a>';
    }

    if ($aRow['recurring'] == 1) {
        $outputName .= '<br /><span class="label label-primary inline-block mtop4"> ' . _l('recurring_task') . '</span>';
    }

    $outputName .= '<div class="row-options">';

    $class = 'text-success bold';
    $style = '';

    $tooltip = '';
    if ($aRow['billed'] == 1 || !$aRow['is_assigned'] || $aRow['status'] == Tasks_model::STATUS_COMPLETE) {
        $class = 'text-dark disabled';
        $style = 'style="opacity:0.6;cursor: not-allowed;"';
        if ($aRow['status'] == Tasks_model::STATUS_COMPLETE) {
            $tooltip = ' data-toggle="tooltip" data-title="' . format_task_status($aRow['status'], false, true) . '"';
        } elseif ($aRow['billed'] == 1) {
            $tooltip = ' data-toggle="tooltip" data-title="' . _l('task_billed_cant_start_timer') . '"';
        } elseif (!$aRow['is_assigned']) {
            $tooltip = ' data-toggle="tooltip" data-title="' . _l('task_start_timer_only_assignee') . '"';
        }
    }

    if ($aRow['not_finished_timer_by_current_staff']) {
        $outputName .= '<a href="#" class="text-danger tasks-table-stop-timer" onclick="timer_action(this,' . $aRow['id'] . ',' . $aRow['not_finished_timer_by_current_staff'] . '); return false;">' . _l('task_stop_timer') . '</a>';
    } else {
        $outputName .= '<span' . $tooltip . ' ' . $style . '>
        <a href="#" class="' . $class . ' tasks-table-start-timer" onclick="timer_action(this,' . $aRow['id'] . '); return false;">' . _l('task_start_timer') . '</a>
        </span>';
    }

    if ($hasPermissionEdit) {
        $outputName .= '<span class="text-dark"> | </span><a href="#" onclick="edit_task(' . $aRow['id'] . '); return false">' . _l('edit') . '</a>';
    }

    if ($hasPermissionDelete) {
        $outputName .= '<span class="text-dark"> | </span><a href="' . admin_url('tasks/delete_task/' . $aRow['id']) . '" class="text-danger _delete task-delete">' . _l('delete') . '</a>';
    }
    $outputName .= '</div>';

    $row[] = $outputName;


    //$creator = get_task_creator($aRow['id']) ;
    //$row[] = _d($aRow['startdate']) ."<br/>" . $creator ;


    $row[] = _d($aRow['duedate']);

    $row[] = format_members_by_ids_and_names($aRow['assignees_ids'], $aRow['assignees']);



    $outputPriority = '<span style="color:' . task_priority_color($aRow['priority']) . ';" class="inline-block">' . task_priority($aRow['priority']);

    if (has_permission('tasks', '', 'edit') && $aRow['status'] != Tasks_model::STATUS_COMPLETE) {
        $outputPriority .= '<div class="dropdown inline-block mleft5 table-export-exclude">';
        $outputPriority .= '<a href="#" style="font-size:14px;vertical-align:middle;" class="dropdown-toggle text-dark" id="tableTaskPriority-' . $aRow['id'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
        $outputPriority .= '<span data-toggle="tooltip" title="' . _l('task_single_priority') . '"><i class="fa fa-caret-down" aria-hidden="true"></i></span>';
        $outputPriority .= '</a>';

        $outputPriority .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="tableTaskPriority-' . $aRow['id'] . '">';
        foreach ($tasksPriorities as $priority) {
            if ($aRow['priority'] != $priority['id']) {
                $outputPriority .= '<li>
                  <a href="#" onclick="task_change_priority(' . $priority['id'] . ',' . $aRow['id'] . '); return false;">
                     ' . $priority['name'] . '
                  </a>
               </li>';
            }
        }
        $outputPriority .= '</ul>';
        $outputPriority .= '</div>';
    }

    $outputPriority .= '</span>';
    $row[] = $outputPriority;

    // Custom fields add values
    foreach ($customFieldsColumns as $customFieldColumn) {
        $row[] = (strpos($customFieldColumn, 'date_picker_') !== false ? _d($aRow[$customFieldColumn]) : $aRow[$customFieldColumn]);
    }

    $row['DT_RowClass'] = 'has-row-options';

    if ((!empty($aRow['duedate']) && $aRow['duedate'] < date('Y-m-d')) && $aRow['status'] != Tasks_model::STATUS_COMPLETE) {
        $row['DT_RowClass'] .= ' text-danger';
    }

    $row = hooks()->apply_filters('tasks_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}

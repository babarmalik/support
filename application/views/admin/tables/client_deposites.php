<?php

defined('BASEPATH') or exit('No direct script access allowed');

$project_id = $this->ci->input->post('project_id');

$aColumns = [
    'id',
    'created_at',
    'amount',
    'company'
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'client_payments';

$join = [
    'JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'client_payments.client'
];

$where  = [];
$filter = [];








if ($clientid != '') {
    array_push($where, 'AND ' . db_prefix() . 'client_payments.client=' . $clientid);
}


//$aColumns = hooks()->apply_filters('invoices_table_sql_columns', $aColumns);



$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);
//echo $this->ci->db->last_query();
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    $numberOutput = '<a href="' . admin_url('client/edit_payment/' . $aRow['id']) . '" target="_blank">' . _d($aRow['created_at']) . '</a>';
    $row[] = $numberOutput;

    $row[] = 'icon';
    $row[] = "task";
    $row[] = app_format_money($aRow['amount'], 'USD');
    $row[] = app_format_money($aRow['amount'], 'USD');
    $row = hooks()->apply_filters('timesheet_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}

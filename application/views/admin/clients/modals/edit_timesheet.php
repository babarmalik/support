<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- Zip Credit Note -->
<div class="modal fade" id="client_timesheet_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('admin/clients/edit_timesheet/'.$client->userid); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo _l('edit_new_timesheet'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mbot15 ">
                            <label for="allowed_payment_modes" class="control-label"><?php echo _l('timesheet_amount'); ?></label>
                            <br />
                            <input required type="number" name="amount" id="amount_timesheet" min="0" value="" class="form-control" placeholder="Amount" aria-invalid="false">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mbot15 ">
                            <label for="allowed_payment_modes" class="control-label"><?php echo _l('timesheet_task'); ?></label>
                            <br />
                            <input required type="text" id="timesheet_description" name="task" class="form-control" placeholder="Description" aria-invalid="false">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="type" value="2">
                <input type="hidden" name="id" id="timesheet_id" value="">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>



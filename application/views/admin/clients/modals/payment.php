<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- Zip Credit Note -->
<div class="modal fade" id="client_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php echo form_open('admin/clients/add_payment/'.$client->userid); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo _l('new_timesheet_payment'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mbot15 ">
                            <label for="allowed_payment_modes" class="control-label"><?php echo _l('timesheet_amount'); ?></label>
                            <br />
                            <input required type="number" name="amount" min="0" value="" class="form-control" placeholder="Amount" aria-invalid="false">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mbot15 ">
                            <label for="allowed_payment_modes" class="control-label"><?php echo _l('timesheet_select_payment'); ?></label>
                            <br />
                            <?php if(count($payment_modes) > 0){ ?>
                                <select name="payment_method" required class="selectpicker"
                                        data-title="<?php echo _l('dropdown_non_selected_tex'); ?>"
                                >
                                    <?php foreach($payment_modes as $mode){
                                        $selected = '';
//                                        if(isset($invoice)){
//                                            if($invoice->allowed_payment_modes){
//                                                $inv_modes = unserialize($invoice->allowed_payment_modes);
//                                                if(is_array($inv_modes)) {
//                                                    foreach($inv_modes as $_allowed_payment_mode){
//                                                        if($_allowed_payment_mode == $mode['id']){
//                                                            $selected = ' selected';
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        } else {
                                        if($mode['selected_by_default'] == 1){
                                            $selected = ' selected';
                                            //}
                                        }
                                        ?>
                                        <option value="<?php echo $mode['id']; ?>"<?php echo $selected; ?>><?php echo $mode['name']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                <p><?php echo _l('timesheet_select_payment'); ?></p>
                                <a class="btn btn-info" href="<?php echo admin_url('paymentmodes'); ?>">
                                    <?php echo _l('new_payment_mode'); ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<?php defined('BASEPATH') or exit('No direct script access allowed');

$table_data = array(
    _l('timesheet_date'),
    _l('timesheet_type'),
    _l('timesheet_task'),
    _l('timesheet_amount'),
    _l('timesheet_balance')
);
$class = "timesheet-single-client";
$table_data = hooks()->apply_filters('timesheet_table_columns', $table_data);
render_datatable($table_data, (isset($class) ? $class : 'timesheet-single-client'));
?>

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    table th{
        cursor: default !important;
    }
    table.dataTable thead .sorting_asc:after {
        content: " ";
    }
    table.dataTable thead .sorting_desc:after {
        content: " ";
    }
    table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc {
        background: #f6f8fa !important;
        font-weight: inherit !important;
    }
    table.dataTable thead .sorting:after {
        opacity: 0;
    }
</style>
<?php if (isset($client)) { ?>
    <h4 class="customer-profile-group-heading"><?php echo _l('timesheet_itle'); ?></h4>

    <a href="#" class="btn btn-info mbot15" data-toggle="modal" data-target="#client_timesheet">
        <?php echo _l('create_new_timesheet'); ?>
    </a>
    <a href="#" class="btn btn-info mbot15" data-toggle="modal" data-target="#client_payment">
        <?php echo _l('new_timesheet_payment'); ?>
    </a>
<!--    <a href="#" class="btn btn-success mbot15 pull-right" data-toggle="modal" data-target="#">-->
<!--        --><?php //echo _l('timesheet_payment'); ?>
<!--    </a>-->
    <div id="" class="mbot20">
        <div class="row">
            <div class="col-lg-4 col-xs-12 col-md-12 total-column">
                <div class="panel_s">
                    <div class="panel-body">
                        <h3 class="text-muted _total">
                            <?php echo app_format_money($total_timesheet_amount["total"], 'USD'); ?> </h3>
                        <span class=""><?php echo _l('timesheet_total'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12 col-md-12 total-column">
                <div class="panel_s">
                    <div class="panel-body">
                        <h3 class="text-muted _total">
                            <?php echo app_format_money($total_timesheet_deposite["total"], 'USD'); ?> </h3>
                        <span class=""><?php echo _l('timesheet_total_deposite'); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12 col-md-12 total-column">
                <div class="panel_s">
                    <div class="panel-body">
                        <h3 class="text-muted _total">
                            <?php echo app_format_money($total_timesheet_balance, 'USD'); ?> </h3>
                        <span class=""><?php echo _l('timesheet_total_balance'); ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
    <?php
    $this->load->view('admin/clients/table_timesheet', array('class' => ''));
    $this->load->view('admin/clients/modals/timesheet');
    $this->load->view('admin/clients/modals/payment');


    $this->load->view('admin/clients/modals/edit_timesheet');
    $this->load->view('admin/clients/modals/edit_payment');
    ?>


<?php } ?>

<script>
    function showEditModal(id, type, amount, task_des) {
        if(type == 2)
        {
            $('#amount_timesheet').val(amount);
            $('#timesheet_description').val(task_des);
            $('#timesheet_id').val(id);
            $('#client_timesheet_edit').modal('show');
            //alert('timesheet');
        }
        else
        {
            $('#amount_payment').val(amount);
            $('#payment_method_edit').val(2);
            $('#payment_method_edit').selectpicker('refresh')
            $('#payment_id').val(id);
            $('#client_payment_edit').modal('show');
            //alert('payment');
        }
    }
</script>

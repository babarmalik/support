<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h4 class="mbot15"><?php echo _l('tasks_summary'); ?></h4>
<style>
    @media only screen and (max-width: 911px) {
       .font-medium{
           font-size: 13px !important;
       }
        .font-medium-xs{
            display: none;
        }
    }
</style>
<div class="row">
  <?php
  $status_text_color = array(
      '1' => ' label-default text-muted ',
      '2' => ' label-warning text-warning ',
      '3' => ' label-testing ',
      '4' => ' label-primary text-primary ',
      '99' => ' label-danger text-danger ',
      '5' => ' label-success text-success ',
  );
  foreach(tasks_summary_data((isset($rel_id) ? $rel_id : null),(isset($rel_type) ? $rel_type : null)) as $summary){
      //print_r($summary);
      ?>
    <div class="col-md-2 col-xs-4 border-right" onclick="filterStatus(<?php echo $summary['status_id']; ?>)" style="cursor: pointer;">
      <h3 class="bold no-mtop"><?php echo $summary['total_tasks']; ?></h3>
        <span class="label inline-block <?php echo $status_text_color[$summary['status_id']] ; ?>" >
        <?php echo $summary['name']; ?>
      </span>
      <p class="font-medium-xs no-mbot text-muted">
        <?php echo _l('tasks_view_assigned_to_user'); ?>: <?php echo $summary['total_my_tasks']; ?>
      </p>
    </div>
    <?php } ?>
  </div>
  <hr class="hr-panel-heading" />

  <script>
      var all_status = [1, 2, 3, 4, 99, 5];
      console.log(all_status);
      function filterStatus(statusID) {

          $.each(all_status, function( index, value ) {
              //console.log( index + ": " + value );
              if (value != statusID)
              {
                  if($( "#task_status_parent_"+value ).hasClass( "active" ))
                  {
                      $( "#task_status_"+value ).trigger( "click" );
                  }

              }
              else
              {
                  if($( "#task_status_parent_"+value ).hasClass( "active" ) == false)
                  {
                      $( "#task_status_"+value ).trigger( "click" );
                  }
              }
          });
      }
  </script>

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="widget relative" id="widget-<?php echo basename(__FILE__, ".php"); ?>"
     data-name="<?php echo _l('quick_stats'); ?>">
    <div class="widget-dragger"></div>
    <div class="row">

        <?php
        $initial_column = 'col-lg-3';
        if (!is_staff_member() && ((!has_permission('invoices', '', 'view') && !has_permission('invoices', '', 'view_own') && (get_option('allow_staff_view_invoices_assigned') == 0
                    || (get_option('allow_staff_view_invoices_assigned') == 1 && !staff_has_assigned_invoices()))))) {
            $initial_column = 'col-lg-6';
        } else if (!is_staff_member() || (!has_permission('invoices', '', 'view') && !has_permission('invoices', '', 'view_own') && (get_option('allow_staff_view_invoices_assigned') == 1 && !staff_has_assigned_invoices()) || (get_option('allow_staff_view_invoices_assigned') == 0 && (!has_permission('invoices', '', 'view') && !has_permission('invoices', '', 'view_own'))))) {
            $initial_column = 'col-lg-4';
        }
        ?>
        <?php if (has_permission('invoices', '', 'view') || has_permission('invoices', '', 'view_own') || (get_option('allow_staff_view_invoices_assigned') == '1' && staff_has_assigned_invoices())) { ?>
            <div class="quick-stats-invoices col-xs-12 col-md-6 col-sm-6 <?php echo $initial_column; ?>">
                <div class="top_stats_wrapper">
                    <a href="<?php echo admin_url('invoices'); ?>" style="text-decoration: none; color: #323a45;">
                        <?php
                        $total_invoices = total_rows(db_prefix() . 'invoices', 'status NOT IN (5,6)' . (!has_permission('invoices', '', 'view') ? ' AND ' . get_invoices_where_sql_for_staff(get_staff_user_id()) : ''));
                        $total_invoices_awaiting_payment = total_rows(db_prefix() . 'invoices', 'status NOT IN (2,5,6)' . (!has_permission('invoices', '', 'view') ? ' AND ' . get_invoices_where_sql_for_staff(get_staff_user_id()) : ''));
                        $percent_total_invoices_awaiting_payment = ($total_invoices > 0 ? number_format(($total_invoices_awaiting_payment * 100) / $total_invoices, 2) : 0);
                        ?>
                        <p class="text-uppercase mtop5"><i
                                    class="hidden-sm fa fa-balance-scale"></i> <?php echo _l('invoices_awaiting_payment'); ?>
                            <span class="pull-right"><?php echo $total_invoices_awaiting_payment; ?> / <?php echo $total_invoices; ?></span>
                        </p>
                        <div class="clearfix"></div>
                        <div class="progress no-margin progress-bar-mini">
                            <div class="progress-bar no-percent-text not-dynamic" role="progressbar"
                                 aria-valuenow="<?php echo $percent_total_invoices_awaiting_payment; ?>"
                                 aria-valuemin="0"
                                 aria-valuemax="100" style="width: 0%"
                                 data-percent="<?php echo $percent_total_invoices_awaiting_payment; ?>">
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
        <?php if (is_staff_member()) {
        if (is_link_allowed(get_staff_user_id(), 'leads_access')) {?>
            <div class="quick-stats-leads col-xs-12 col-md-6 col-sm-6 <?php echo $initial_column; ?>">
                <div class="top_stats_wrapper">
                    <a href="<?php echo admin_url('leads'); ?>" style="text-decoration: none; color: #323a45;">
                        <?php
                        $where = '';
                        if (!is_admin()) {
                            $where .= '(addedfrom = ' . get_staff_user_id() . ' OR assigned = ' . get_staff_user_id() . ')';
                        }
                        // Junk leads are excluded from total
                        $total_leads = total_rows(db_prefix() . 'leads', ($where == '' ? 'junk=0' : $where .= ' AND junk =0'));
                        if ($where == '') {
                            $where .= 'status=1';
                        } else {
                            $where .= ' AND status =1';
                        }
                        $total_leads_converted = total_rows(db_prefix() . 'leads', $where);
                        $percent_total_leads_converted = ($total_leads > 0 ? number_format(($total_leads_converted * 100) / $total_leads, 2) : 0);
                        ?>
                        <p class="text-uppercase mtop5"><i
                                    class="hidden-sm fa fa-tty"></i> <?php echo _l('leads_converted_to_client'); ?>
                            <span class="pull-right"><?php echo $total_leads_converted; ?> / <?php echo $total_leads; ?></span>
                        </p>
                        <div class="clearfix"></div>
                        <div class="progress no-margin progress-bar-mini">
                            <div class="progress-bar progress-bar-success no-percent-text not-dynamic"
                                 role="progressbar"
                                 aria-valuenow="<?php echo $percent_total_leads_converted; ?>" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 0%"
                                 data-percent="<?php echo $percent_total_leads_converted; ?>">
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } } ?>

        <div class="quick-stats-tasks col-xs-12 col-md-6 col-sm-6 <?php echo $initial_column; ?>">
            <div class="top_stats_wrapper">
                <a href="<?php echo admin_url('tasks'); ?>" style="text-decoration: none; color: #323a45;">
                    <?php
                    $_where = '';
                    if (!has_permission('tasks', '', 'view')) {
                        $_where = db_prefix() . 'tasks.id IN (SELECT taskid FROM ' . db_prefix() . 'task_assigned WHERE staffid = ' . get_staff_user_id() . ')';
                    }
                    $total_tasks = total_rows(db_prefix() . 'tasks', $_where);
                    $where = ($_where == '' ? '' : $_where . ' AND ') . 'status != ' . Tasks_model::STATUS_COMPLETE;
                    $total_not_finished_tasks = total_rows(db_prefix() . 'tasks', $where);
                    $percent_not_finished_tasks = ($total_tasks > 0 ? number_format(($total_not_finished_tasks * 100) / $total_tasks, 2) : 0);
                    ?>
                    <p class="text-uppercase mtop5"><i
                                class="hidden-sm fa fa-tasks"></i> <?php echo _l('tasks_not_finished'); ?> <span
                                class="pull-right">
                  <?php echo $total_not_finished_tasks; ?> / <?php echo $total_tasks; ?>
                  </span>
                    </p>
                    <div class="clearfix"></div>
                    <div class="progress no-margin progress-bar-mini">
                        <div class="progress-bar progress-bar-default no-percent-text not-dynamic" role="progressbar"
                             aria-valuenow="<?php echo $percent_not_finished_tasks; ?>" aria-valuemin="0"
                             aria-valuemax="100" style="width: 0%"
                             data-percent="<?php echo $percent_not_finished_tasks; ?>">
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php if (is_link_allowed(get_staff_user_id(), 'support_access')) { ?>
        <div class="quick-stats-invoices col-xs-12 <?php echo $initial_column; ?> col-sm-6">
            <div class="top_stats_wrapper">
                <a href="<?php echo admin_url('tickets') . '?status=1' ?>">

                    <p class="text-uppercase mtop5 "><i
                                class="hidden-sm "></i> <?php echo _l('tickets');; ?>
                        <span class="pull-right"><?php echo total_rows(db_prefix() . 'tickets', 'status=1'); ?> / <?php echo total_rows(db_prefix() . 'tickets', 'status=5'); ?></span>
                    </p>
                    <div class="clearfix"></div>
                    <div class="progress no-margin progress-bar-mini">
                        <div class="progress-bar no-percent-text not-dynamic" role="progressbar"
                             aria-valuenow="<?php echo total_rows(db_prefix() . 'tickets', 'status=1'); ?>"
                             aria-valuemin="0"
                             aria-valuemax="100" style="width: 0%"
                             data-percent="<?php echo total_rows(db_prefix() . 'tickets', 'status=5'); ?>">
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
